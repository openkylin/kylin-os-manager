/*
 * kylin-os-manager
 *
 * Copyright (C) 2022, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLEARCOMMON_H
#define CLEARCOMMON_H
namespace KylinRubbishClear
{

const int MainTitleHeight = 33;
const int MainNaviWidth = 200;
const int LabelSlogenX = 248 - MainNaviWidth;
const int LabelSlogenY = 88 - MainTitleHeight;
const int LabelSlogenWidth = 432;
const int LabelSlogenHeight = 44;

} // namespace KylinRubbishClear
#endif // CLEARCOMMON_H
