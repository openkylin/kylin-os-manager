<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="106"/>
        <location filename="../cleandetailveiw.cpp" line="111"/>
        <location filename="../cleandetailveiw.cpp" line="288"/>
        <source>Computer scan in progress...</source>
        <translation>ᠶᠠᠭ ᠰᠢᠷᠪᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="114"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="120"/>
        <source>Cleanup</source>
        <translation>ᠨᠢᠭᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="130"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="137"/>
        <source>Finish</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="177"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>System cache</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="185"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠪᠠᠭᠯᠠᠭ᠎ᠠ᠂ ᠪᠠᠭᠠᠰᠬᠠᠭᠰᠠᠨ ᠵᠢᠷᠤᠭ᠂ ᠬᠠᠢᠭᠴᠢ ᠵᠢᠨ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠵᠡᠷᠭᠡ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="196"/>
        <location filename="../cleandetailveiw.cpp" line="236"/>
        <source>Details</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠭᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="220"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="862"/>
        <source>Clearance completed</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠬᠤ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="863"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤᠯᠠᠮ ᠴᠡᠪᠡᠷᠬᠡᠨ ᠪᠣᠯᠤᠭᠰᠠᠨ᠂ ᠦᠷᠬᠦᠯᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠳᠡᠭ ᠵᠤᠷᠰᠢᠯ ᠵᠢᠨᠨ ᠪᠠᠷᠢᠮᠳᠠᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="259"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Historical trace</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="341"/>
        <location filename="../cleandetailveiw.cpp" line="824"/>
        <source>Computer cleanup in progress...</source>
        <translation>ᠶᠠᠭ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="465"/>
        <location filename="../cleandetailveiw.cpp" line="467"/>
        <source>Cleanable cache </source>
        <translation>ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source> items</source>
        <translation> ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="534"/>
        <source> historical use traces</source>
        <translation> ᠲᠡᠤᠬᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="593"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <location filename="../cleandetailveiw.cpp" line="644"/>
        <source>Cleanable Cache</source>
        <translation>ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <location filename="../cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cookie</source>
        <translation>Cookie ᠢ᠋/ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="700"/>
        <location filename="../cleandetailveiw.cpp" line="702"/>
        <source>Clear cache </source>
        <translation>ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source>Clear cookie </source>
        <translation>cookie ᠢ᠋/ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="228"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠭᠠᠷᠤᠭᠰᠠᠨ᠂ ᠳᠤᠭᠯᠠᠭᠠᠮ ᠲᠣᠭᠯᠠᠭᠰᠠᠨ᠂ ᠪᠠᠷᠠᠭ᠎ᠠ ᠬᠤᠳᠠᠯᠳᠤᠨ ᠠᠪᠤᠭᠰᠠᠨ ᠵᠡᠷᠭᠡ ᠮᠥᠷ ᠵᠢᠨᠨᠴᠡᠪᠡᠷᠯᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="267"/>
        <source>Clear system usage traces</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠤᠯᠠ ᠮᠦᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <source>Cleanable browser </source>
        <translation>ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>system cache</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>cookie record</source>
        <translation>Cookie ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="588"/>
        <source>history trace</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item,</source>
        <translation> ᠳᠦᠷᠦᠯ,</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="600"/>
        <location filename="../cleandetailveiw.cpp" line="615"/>
        <source> item</source>
        <translation> ᠵᠤᠷᠪᠤᠰ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="603"/>
        <source>Complete</source>
        <translation>ᠰᠢᠷᠪᠢᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source>Clear </source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source> historical traces</source>
        <translation> ᠲᠡᠤᠬᠡᠨ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="837"/>
        <location filename="../cleandetailveiw.cpp" line="840"/>
        <location filename="../cleandetailveiw.cpp" line="843"/>
        <location filename="../cleandetailveiw.cpp" line="971"/>
        <location filename="../cleandetailveiw.cpp" line="972"/>
        <location filename="../cleandetailveiw.cpp" line="973"/>
        <source>Cleaning up......</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ......</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="956"/>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <source>Cleaning up</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="961"/>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <source>Cleaning up..</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ..</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="966"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <source>Cleaning up....</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ....</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="82"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠪᠡᠯ ᠲᠠᠨ ᠤ᠋ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠬᠤᠷᠳᠤᠨ ᠮᠦᠷᠳᠡᠬᠡᠨ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠪᠠᠢᠵᠤ ᠴᠢᠳᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="160"/>
        <source>StartClear</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠰᠢᠷᠪᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <source>Cleanup Package Cache</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭᠠᠨ ᠤ᠋ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠭᠰᠠᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <location filename="../clearmainwidget.cpp" line="359"/>
        <source>Cleanup FireFox Cache</source>
        <translation>FireFox ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <location filename="../clearmainwidget.cpp" line="417"/>
        <source>Cleanup Chromium Cache</source>
        <translation>Chromium ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="253"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <location filename="../clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>ᠴᠢ ᠠᠨ ᠰᠢᠨ ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="73"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠬᠣᠭ ᠨᠢᠭᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <location filename="../clearmainwidget.cpp" line="297"/>
        <source>System cache</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="109"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠪᠠᠭᠠᠯᠠᠭ᠎ᠠ᠂ ᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠵᠢᠷᠤᠭ ᠵᠢᠴᠢ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠵᠡᠷᠭᠡ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <location filename="../clearmainwidget.cpp" line="312"/>
        <source>Cookies</source>
        <translation>cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="119"/>
        <source>Clean up Internet history,cookies</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠮᠥᠷ᠂cookies ᠵᠡᠷᠭᠡ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="128"/>
        <location filename="../clearmainwidget.cpp" line="327"/>
        <source>History trace</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="129"/>
        <source>Clean up system usage traces</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠣᠷᠣᠮ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="254"/>
        <source>Cleanup Trash Box</source>
        <translation>ᠬᠤᠷᠢᠶᠠᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="350"/>
        <location filename="../clearmainwidget.cpp" line="363"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>Firefox ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <location filename="../clearmainwidget.cpp" line="421"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>Chromium ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="379"/>
        <location filename="../clearmainwidget.cpp" line="392"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>ᠴᠢ ᠠᠨ ᠰᠢᠨ ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="412"/>
        <location filename="../clearmainwidget.cpp" line="425"/>
        <source>Clean up the Chromium Internet records</source>
        <translation>清理Chromium浏览器上网记录</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="268"/>
        <source>Clean up the recently opened documents records</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠡᠭᠦᠯ ᠳ᠋ᠤ᠌ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠲᠤᠺᠦᠢᠮᠸᠨ᠋ᠲ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="269"/>
        <source>Delete the command history</source>
        <translation>ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠲᠡᠤᠬᠡᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠫᠷᠤᠵᠸᠺᠲ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠫᠷᠤᠵᠸᠺᠲ:</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠫᠷᠤᠵᠸᠺᠲ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation>ᠬᠤᠭ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
</context>
</TS>
