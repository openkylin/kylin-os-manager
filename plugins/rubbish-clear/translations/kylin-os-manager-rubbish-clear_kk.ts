<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="106"/>
        <location filename="../cleandetailveiw.cpp" line="111"/>
        <location filename="../cleandetailveiw.cpp" line="288"/>
        <source>Computer scan in progress...</source>
        <translation>Компьютерді сканерлеу жүріп жатыр...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="114"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="120"/>
        <source>Cleanup</source>
        <translation>Тазалау</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="130"/>
        <source>Return</source>
        <translation>Қайтарым</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="137"/>
        <source>Finish</source>
        <translation>Аяқтау</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="177"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>System cache</source>
        <translation>Жүйелік кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="185"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>Буманың белгісін және браузер кэшін тазалау</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="196"/>
        <location filename="../cleandetailveiw.cpp" line="236"/>
        <source>Details</source>
        <translation>Егжей- тегжей</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="220"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Cookies</source>
        <translation>Cookie файлдары</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="862"/>
        <source>Clearance completed</source>
        <translation>Рұқсат беру аяқталды</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="863"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>Компьютер өте жігерлі, тазалау әдет-ғұрпын сақтаңыздар</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="259"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Historical trace</source>
        <translation>Тарихи із</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="341"/>
        <location filename="../cleandetailveiw.cpp" line="824"/>
        <source>Computer cleanup in progress...</source>
        <translation>Компьютерді тазалау жүріп жатыр...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="465"/>
        <location filename="../cleandetailveiw.cpp" line="467"/>
        <source>Cleanable cache </source>
        <translation>Тазаланатын кэш </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source> items</source>
        <translation> элементтер</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="534"/>
        <source> historical use traces</source>
        <translation> тарихи қолданыс іздері</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="593"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>Тазалауға ештеңе жоқ.</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <location filename="../cleandetailveiw.cpp" line="644"/>
        <source>Cleanable Cache</source>
        <translation>Тазартқыш кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <location filename="../cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cookie</source>
        <translation>Таза cookie</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="700"/>
        <location filename="../cleandetailveiw.cpp" line="702"/>
        <source>Clear cache </source>
        <translation>Кэшті тазалау </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source>Clear cookie </source>
        <translation>Cookie файлын тазалау </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="228"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>Интернетті тазалау]ойындар]сауда тарихы және т.б</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="267"/>
        <source>Clear system usage traces</source>
        <translation>Браузерді және жүйелік пайдалану іздерін тазалау</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <source>Cleanable browser </source>
        <translation>Таза браузер </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>system cache</source>
        <translation>жүйелік кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>cookie record</source>
        <translation>cookie жазбасы</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="588"/>
        <source>history trace</source>
        <translation>журнал ізі</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item,</source>
        <translation> элемент,</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="600"/>
        <location filename="../cleandetailveiw.cpp" line="615"/>
        <source> item</source>
        <translation> элемент</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="603"/>
        <source>Complete</source>
        <translation>Аяқтау</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source>Clear </source>
        <translation>Тазалау </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source> historical traces</source>
        <translation> тарихи іздер</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="837"/>
        <location filename="../cleandetailveiw.cpp" line="840"/>
        <location filename="../cleandetailveiw.cpp" line="843"/>
        <location filename="../cleandetailveiw.cpp" line="971"/>
        <location filename="../cleandetailveiw.cpp" line="972"/>
        <location filename="../cleandetailveiw.cpp" line="973"/>
        <source>Cleaning up......</source>
        <translation>Тазалау...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="956"/>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <source>Cleaning up</source>
        <translation>Тазалау</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="961"/>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <source>Cleaning up..</source>
        <translation>Тазалау...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="966"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <source>Cleaning up....</source>
        <translation>Тазалау...</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="82"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>Компьютердің жарық және қауіпсіз болу үшін үнемі тазалап отыру</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <location filename="../clearmainwidget.cpp" line="297"/>
        <source>System cache</source>
        <translation type="unfinished">Жүйелік кэш</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <location filename="../clearmainwidget.cpp" line="312"/>
        <source>Cookies</source>
        <translation>Cookie файлдары</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="128"/>
        <location filename="../clearmainwidget.cpp" line="327"/>
        <source>History trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="160"/>
        <source>StartClear</source>
        <translation>себетті</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <source>Cleanup Package Cache</source>
        <translation>Тазалау пакет кэші</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>Нобайларды тазалау кэші</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="253"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <location filename="../clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>Qaxbrowser кэшін тазалау</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="254"/>
        <source>Cleanup Trash Box</source>
        <translation>себетті тазалау</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="379"/>
        <location filename="../clearmainwidget.cpp" line="392"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>Qaxbrowser бағдарламасында «Cookie» файлдарын сақтауды тазалау</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="350"/>
        <location filename="../clearmainwidget.cpp" line="363"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>Firefox бағдарламасында «Cookie» файлдарын сақтауды тазалау</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="268"/>
        <source>Clean up the recently opened documents records</source>
        <translation>Жақында ашылған құжаттар жазбаларын тазалау</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="269"/>
        <source>Delete the command history</source>
        <translation>Пәрмен журналын жою</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <location filename="../clearmainwidget.cpp" line="359"/>
        <source>Cleanup FireFox Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <location filename="../clearmainwidget.cpp" line="417"/>
        <source>Cleanup Chromium Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <location filename="../clearmainwidget.cpp" line="421"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="412"/>
        <location filename="../clearmainwidget.cpp" line="425"/>
        <source>Clean up the Chromium Internet records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="73"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>Компьютерлік қоқыс тазалаудың бір кілті</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="109"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>Пакеттерді, нобайларды және себетті тазалау</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="119"/>
        <source>Clean up Internet history,cookies</source>
        <translation>Интернет тарихын, cookie файлдарын тазалау</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="129"/>
        <source>Clean up system usage traces</source>
        <translation>Браузерді және жүйелік пайдалану іздерін тазалау</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>Тазаланатын элементтер таңдалмады!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>сенімді</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>Элементтерді тазалау:</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>Тазалауға арналған заттар жоқ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
