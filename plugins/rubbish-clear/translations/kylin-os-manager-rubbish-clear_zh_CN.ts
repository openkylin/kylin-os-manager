<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="106"/>
        <location filename="../cleandetailveiw.cpp" line="111"/>
        <location filename="../cleandetailveiw.cpp" line="288"/>
        <source>Computer scan in progress...</source>
        <translation>正在扫描中...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="114"/>
        <source>Cancel</source>
        <translation>取消扫描</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="120"/>
        <source>Cleanup</source>
        <translation>一键清理</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="130"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="137"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="177"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>System cache</source>
        <translation>系统缓存</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="185"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>清理包、缩略图和浏览器缓存等</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="196"/>
        <location filename="../cleandetailveiw.cpp" line="236"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="220"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="862"/>
        <source>Clearance completed</source>
        <translation>清理完成</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="863"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>电脑越来越干净啦，请保持常清理习惯</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="259"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Historical trace</source>
        <translation>历史痕迹</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="341"/>
        <location filename="../cleandetailveiw.cpp" line="824"/>
        <source>Computer cleanup in progress...</source>
        <translation>正在清理中...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="465"/>
        <location filename="../cleandetailveiw.cpp" line="467"/>
        <source>Cleanable cache </source>
        <translation>可清理缓存 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source> items</source>
        <translation> 个</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="534"/>
        <source> historical use traces</source>
        <translation> 历史使用痕迹</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="593"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>无可清理内容</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <location filename="../cleandetailveiw.cpp" line="644"/>
        <source>Cleanable Cache</source>
        <translation>可清理缓存</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <location filename="../cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cookie</source>
        <translation>可清理 Cookie</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="700"/>
        <location filename="../cleandetailveiw.cpp" line="702"/>
        <source>Clear cache </source>
        <translation>清理缓存 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source>Clear cookie </source>
        <translation>清理 Cookie </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="228"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>清理上网、游戏、购物等历史</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="267"/>
        <source>Clear system usage traces</source>
        <translation>清理系统使用痕迹</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <source>Cleanable browser </source>
        <translation>可清理浏览器 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>system cache</source>
        <translation>系统缓存 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>cookie record</source>
        <translation>Cookie 记录 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="588"/>
        <source>history trace</source>
        <translation>历史痕迹 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item,</source>
        <translation> 项，</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="600"/>
        <location filename="../cleandetailveiw.cpp" line="615"/>
        <source> item</source>
        <translation> 条</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="603"/>
        <source>Complete</source>
        <translation>扫描完成</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source>Clear </source>
        <translation>清理 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source> historical traces</source>
        <translation> 历史痕迹</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="837"/>
        <location filename="../cleandetailveiw.cpp" line="840"/>
        <location filename="../cleandetailveiw.cpp" line="843"/>
        <location filename="../cleandetailveiw.cpp" line="971"/>
        <location filename="../cleandetailveiw.cpp" line="972"/>
        <location filename="../cleandetailveiw.cpp" line="973"/>
        <source>Cleaning up......</source>
        <translation>清理中...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="956"/>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <source>Cleaning up</source>
        <translation>清理</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="961"/>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <source>Cleaning up..</source>
        <translation>清理中...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="966"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <source>Cleaning up....</source>
        <translation>清理中...</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="82"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>经常清理，让您的电脑又轻快又安全</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <location filename="../clearmainwidget.cpp" line="297"/>
        <source>System cache</source>
        <translation>系统缓存</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <location filename="../clearmainwidget.cpp" line="312"/>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="128"/>
        <location filename="../clearmainwidget.cpp" line="327"/>
        <source>History trace</source>
        <translation>历史痕迹</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="160"/>
        <source>StartClear</source>
        <translation>开始扫描</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <source>Cleanup Package Cache</source>
        <translation>清理安装包缓存</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>清理缩略图</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="253"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <location filename="../clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>清理奇安信浏览器</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="254"/>
        <source>Cleanup Trash Box</source>
        <translation>清理回收站</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="379"/>
        <location filename="../clearmainwidget.cpp" line="392"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>清理奇安信浏览器 Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="350"/>
        <location filename="../clearmainwidget.cpp" line="363"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>清理火狐浏览器 Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="268"/>
        <source>Clean up the recently opened documents records</source>
        <translation>清理最近打开的文档记录</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="269"/>
        <source>Delete the command history</source>
        <translation>清理命令行历史记录</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <location filename="../clearmainwidget.cpp" line="359"/>
        <source>Cleanup FireFox Cache</source>
        <translation>清理火狐浏览器</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <location filename="../clearmainwidget.cpp" line="417"/>
        <source>Cleanup Chromium Cache</source>
        <translation>清理谷歌浏览器</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <location filename="../clearmainwidget.cpp" line="421"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>清理谷歌浏览器 Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="412"/>
        <location filename="../clearmainwidget.cpp" line="425"/>
        <source>Clean up the Chromium Internet records</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="73"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>电脑垃圾    一键清理</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="109"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>清理包，缩略图及回收站等</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="119"/>
        <source>Clean up Internet history,cookies</source>
        <translation>清理上网历史，Cookies 等</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="129"/>
        <source>Clean up system usage traces</source>
        <translation>清理系统使用痕迹</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>未选择可清理的项目！</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>清理项目：</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>没有清理项</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation>垃圾清理</translation>
    </message>
</context>
</TS>
