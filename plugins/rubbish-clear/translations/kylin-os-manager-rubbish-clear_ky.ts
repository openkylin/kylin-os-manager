<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="106"/>
        <location filename="../cleandetailveiw.cpp" line="111"/>
        <location filename="../cleandetailveiw.cpp" line="288"/>
        <source>Computer scan in progress...</source>
        <translation>Компьютердик сканерлеу жүрүшүндө...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="114"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="120"/>
        <source>Cleanup</source>
        <translation>Тазалоо</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="130"/>
        <source>Return</source>
        <translation>Кайтып келүү</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="137"/>
        <source>Finish</source>
        <translation>Бүтүрүү</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="177"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>System cache</source>
        <translation>Системалык кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="185"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>Так пакети миниатюралар жана браузер кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="196"/>
        <location filename="../cleandetailveiw.cpp" line="236"/>
        <source>Details</source>
        <translation>Майда-чүйдөсүнө</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="220"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Cookies</source>
        <translation>Кукилер</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="862"/>
        <source>Clearance completed</source>
        <translation>Тазалоо аяктады</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="863"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>Компьютер абдан энергиялуу, сураныч, тазалоо адаттарын сактоо</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="259"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Historical trace</source>
        <translation>Тарыхый изи</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="341"/>
        <location filename="../cleandetailveiw.cpp" line="824"/>
        <source>Computer cleanup in progress...</source>
        <translation>Компьютердик тазалоо жүрүшүндө...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="465"/>
        <location filename="../cleandetailveiw.cpp" line="467"/>
        <source>Cleanable cache </source>
        <translation>Таза кэш </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source> items</source>
        <translation> элементтер</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="534"/>
        <source> historical use traces</source>
        <translation> тарыхый колдонуу изи</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="593"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>Тазалоо үчүн эч нерсе жок.</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <location filename="../cleandetailveiw.cpp" line="644"/>
        <source>Cleanable Cache</source>
        <translation>Таза кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <location filename="../cleandetailveiw.cpp" line="658"/>
        <source>Cleanable Cookie</source>
        <translation>Таза куки</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="700"/>
        <location filename="../cleandetailveiw.cpp" line="702"/>
        <source>Clear cache </source>
        <translation>Кэшти тазалоо </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="674"/>
        <source>Clear cookie </source>
        <translation>Ачык-айкын куки </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="228"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>Ачык интернет оюндар соода тарыхы ж.б</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="267"/>
        <source>Clear system usage traces</source>
        <translation>Жүйені пайдалануды бақылауды тазалаңыз</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="473"/>
        <source>Cleanable browser </source>
        <translation>Тазалоочу браузер </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>system cache</source>
        <translation>система кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>cookie record</source>
        <translation>куки рекорду</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="588"/>
        <source>history trace</source>
        <translation>тарых изи</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item,</source>
        <translation> элемент,</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="600"/>
        <location filename="../cleandetailveiw.cpp" line="615"/>
        <source> item</source>
        <translation> элемент</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="603"/>
        <source>Complete</source>
        <translation>Толук</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source>Clear </source>
        <translation>Тазалоо </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="741"/>
        <source> historical traces</source>
        <translation> тарыхый изи</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="837"/>
        <location filename="../cleandetailveiw.cpp" line="840"/>
        <location filename="../cleandetailveiw.cpp" line="843"/>
        <location filename="../cleandetailveiw.cpp" line="971"/>
        <location filename="../cleandetailveiw.cpp" line="972"/>
        <location filename="../cleandetailveiw.cpp" line="973"/>
        <source>Cleaning up......</source>
        <translation>Тазалоо ......</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="956"/>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <source>Cleaning up</source>
        <translation>Тазалоо</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="961"/>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <source>Cleaning up..</source>
        <translation>Тазалоо..</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="966"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <source>Cleaning up....</source>
        <translation>Тазалоо....</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="82"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>Компьютердин жарыгын жана коопсуздугун сактоо үчүн дайыма тазалагыла</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <location filename="../clearmainwidget.cpp" line="297"/>
        <source>System cache</source>
        <translation type="unfinished">Системалык кэш</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <location filename="../clearmainwidget.cpp" line="312"/>
        <source>Cookies</source>
        <translation>Кукилер</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="128"/>
        <location filename="../clearmainwidget.cpp" line="327"/>
        <source>History trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="160"/>
        <source>StartClear</source>
        <translation>StartClear</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <source>Cleanup Package Cache</source>
        <translation>Тазалоо пакети Кэш</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>Тазалоо Миниатюра Кэш</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="253"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <location filename="../clearmainwidget.cpp" line="388"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>Тазалоо Каксброузер Кэш</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <location filename="../clearmainwidget.cpp" line="244"/>
        <location filename="../clearmainwidget.cpp" line="254"/>
        <source>Cleanup Trash Box</source>
        <translation>тазалоо таштанды кутуча</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="379"/>
        <location filename="../clearmainwidget.cpp" line="392"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>Касброузердеги кукилерди тазалоо</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="261"/>
        <location filename="../clearmainwidget.cpp" line="350"/>
        <location filename="../clearmainwidget.cpp" line="363"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>Отко үнөмдөлгөн кукилер тазалоо</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="268"/>
        <source>Clean up the recently opened documents records</source>
        <translation>Жакында ачылган документтердин жазууларын тазалоо</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="269"/>
        <source>Delete the command history</source>
        <translation>Команда тарыхын жоготуу</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <location filename="../clearmainwidget.cpp" line="359"/>
        <source>Cleanup FireFox Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <location filename="../clearmainwidget.cpp" line="417"/>
        <source>Cleanup Chromium Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <location filename="../clearmainwidget.cpp" line="421"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="412"/>
        <location filename="../clearmainwidget.cpp" line="425"/>
        <source>Clean up the Chromium Internet records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="73"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>Компьютердик таштандылар тазалоо үчүн бир ачкыч</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="109"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>Пакеттерди, миниатюраларды тазалап, бинтти кайра иштетүү</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="119"/>
        <source>Clean up Internet history,cookies</source>
        <translation>Интернет тарыхын тазалоо,кукилер</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="129"/>
        <source>Clean up system usage traces</source>
        <translation>Жүйені пайдалануды бақылауды тазалаңыз</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>Тазалоочу заттар тандалган жок!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>албетте</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>Таза заттар:</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>Тазалоо үчүн эч кандай буюмдар</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
