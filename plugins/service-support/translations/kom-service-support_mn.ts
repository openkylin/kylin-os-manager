<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="35"/>
        <source>select detailed category</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="39"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System activation</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System installation</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System crash</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠭᠠᠯᠵᠠᠭᠤᠷᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System performance</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠴᠢᠳᠠᠪᠬᠢ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>Control center</source>
        <translation>ᠡᠵᠡᠮᠳᠡᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System setting</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="43"/>
        <source>System basis consulting</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠠᠭᠤᠷᠢ ᠯᠠᠪᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="51"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>ᠲᠠ ᠡᠨᠳᠡ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠲᠠᠯ᠎ᠠ ᠪᠡᠷ ᠬᠢ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ᠂ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠡᠷᠢᠵᠤ ᠣᠯᠬᠤ ᠥᠬᠡᠢ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠤᠪᠢ ᠵᠢᠨ ᠴᠢᠳᠠᠪᠬᠢ ᠳᠤᠳᠤᠷᠬᠠᠢ ᠥᠬᠡᠢ ᠬᠡᠬᠦ ᠮᠡᠳᠦ ᠪᠡᠷ᠃ ᠲᠠ ᠳᠤᠬᠢᠶᠠᠯᠳᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠯᠢᠭ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠵᠤ᠂ ᠪᠠᠰᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠦᠵᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ. </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="64"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>ᠲᠠ ᠡᠨᠳᠡ ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠲᠠᠯ᠎ᠠ ᠪᠡᠷ ᠬᠢ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ᠂ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ᠂ ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠠᠪᠴᠠᠯᠳᠤᠯ ᠬᠡᠬᠦ ᠮᠡᠳᠦ ᠪᠡᠷ᠃ ᠲᠠ ᠳᠤᠬᠢᠶᠠᠯᠳᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠯᠢᠭ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠵᠤ᠂ ᠪᠠᠰᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠦᠵᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ. </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="75"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>ᠲᠠ ᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠲᠠᠯ᠎ᠠ ᠪᠡᠷ ᠬᠢ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠴᠢ ᠯᠢᠨ ᠤ᠋ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠰᠣᠹᠲ ᠢ᠋ ᠣᠯᠵᠠᠯᠠᠬᠤ᠂ ᠤᠭᠰᠠᠷᠬᠤ᠂ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠳ᠋ᠤ᠌ ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠬᠡᠬᠦ ᠮᠡᠳᠦ ᠪᠡᠷ᠃ ᠲᠠ ᠳᠤᠬᠢᠶᠠᠯᠳᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠯᠢᠭ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠵᠤ᠂ ᠪᠠᠰᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠦᠵᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ. </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="85"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>ᠲᠠ ᠡᠨᠳᠡ ᠪᠤᠰᠤᠳ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠬᠤ ᠪᠤᠶᠤ ᠪᠢᠳᠡᠨ ᠳ᠋ᠤ᠌ ᠡᠷᠬᠢᠮ ᠰᠠᠨᠠᠯ ᠵᠦᠪᠯᠡᠯᠬᠡ ᠪᠡᠨ ᠦᠭᠴᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral</source>
        <translation>ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠯᠠᠪᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral adaptation consulting</source>
        <translation>ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠠᠪᠴᠠᠯᠳᠤᠯ ᠤ᠋ᠨ ᠯᠠᠪᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral driver acquisition</source>
        <translation>ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠢ᠋ ᠣᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="60"/>
        <source>Peripheral use and error reporting</source>
        <translation>ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠬᠢᠭᠡᠳ ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="70"/>
        <source>Application</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠰᠣᠹᠲ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software installation and uninstallation</source>
        <translation>ᠰᠣᠹᠲ ᠤᠭᠰᠠᠷᠬᠤ ᠬᠢᠬᠡᠳ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software use and error reporting</source>
        <translation>ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠬᠢᠬᠡᠳ ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="81"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="83"/>
        <source>Opinions and suggestions</source>
        <translation>ᠰᠠᠨᠠᠯ ᠵᠥᠪᠯᠡᠯᠭᠡ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="149"/>
        <source>Attachment size exceeds limit!</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠢᠵᠠᠭᠠᠷ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ!</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="61"/>
        <source>Failed to create temporary directory!</source>
        <translation>ᠲᠦᠷ ᠴᠠᠭ ᠤ᠋ᠨ ᠭᠠᠷᠴᠠᠭ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>ᠦᠰᠦᠷᠦᠭᠡᠳ</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>ᠨᠡᠢᠲᠡ</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>ᠦᠰᠦᠷᠦᠭᠡᠳ</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="191"/>
        <source>System log</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="192"/>
        <source>Machine</source>
        <translation>ᠪᠦᠳᠦᠨ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="193"/>
        <source>Hardware</source>
        <translation>ᠬᠠᠷᠳ᠋ᠸᠠᠢᠷ ᠤ᠋ᠨ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="194"/>
        <source>Drive</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="195"/>
        <source>APP list</source>
        <translation>ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="196"/>
        <source>Rules</source>
        <translation>ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠠᠷᠭ᠎ᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="197"/>
        <source>Network</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="198"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
</context>
<context>
    <name>UIMainPage</name>
    <message>
        <location filename="../UI/uimainpage.cpp" line="81"/>
        <source>ServiceSupport</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="84"/>
        <source>Multi-channel technical support services</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠳᠡᠮᠵᠢᠬ</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="87"/>
        <source>Feedback</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤ ᠲᠤᠰᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="90"/>
        <source>Self service</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="103"/>
        <source>website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="93"/>
        <source>History</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠤ᠋ᠨ ᠳᠡᠤᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="102"/>
        <source>Jump to</source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠦᠰᠦᠷᠦᠬᠡᠳ</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="110"/>
        <source> to get more services</source>
        <translation> ᠨᠡᠩ ᠣᠯᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠤ᠋ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠤᠢᠯᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Creation time</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Type</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠲᠥᠷᠥᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="71"/>
        <source>bydesign</source>
        <translation>ᠵᠢᠷᠤᠭ ᠳᠦᠯᠦᠪᠯᠡᠬᠡ ᠡᠨᠡ ᠮᠡᠳᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="73"/>
        <source>duplicate</source>
        <translation>ᠳᠠᠪᠳᠠᠭᠳᠠᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="75"/>
        <source>external</source>
        <translation>ᠭᠠᠳᠠᠷ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠰᠢᠯᠳᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="77"/>
        <source>fixed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="79"/>
        <source>notrepro</source>
        <translation>ᠳᠠᠬᠢᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="81"/>
        <source>postponed</source>
        <translation>ᠬᠤᠢᠰᠢᠯᠠᠭᠤᠯᠵᠤ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="83"/>
        <source>willnotfix</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠵᠤ ᠥᠭᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="99"/>
        <source>verify</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="102"/>
        <source>Has the issue been resolved?</source>
        <translation>ᠡᠨᠡ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭ᠍ᠰᠡᠨ ᠡᠰᠡᠬᠦ ᠶᠢ ᠨᠤᠲᠠᠯᠠᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠤ ?</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="105"/>
        <source>Once identified, the issue will be closed and no further action will be taken.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠰᠢᠶᠳᠪᠦᠷᠢᠯᠡᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠲᠤᠰ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ ᠬᠠᠭᠠᠭᠰᠠᠨ ᠮᠥᠷᠲᠡᠭᠡᠨ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠦᠨ ᠬᠤᠪᠴᠠᠰᠤ ᠶᠢᠨ ᠠᠵᠢᠯᠲᠠᠳ ᠢ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠳᠠᠭᠠᠬᠤ ᠦᠭᠡᠶ.</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="107"/>
        <source>resolved</source>
        <translation>ᠰᠢᠶᠳᠪᠦᠷᠢᠯᠡᠨ᠎</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="109"/>
        <source>cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Description</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠲᠣᠭᠠᠴᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>Progress</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠠᠬᠢᠴᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="194"/>
        <source>No record</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠭᠰᠠᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="195"/>
        <source>There is a network problem, please try again later</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠭᠠᠷᠪᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="196"/>
        <source>Loading, please wait</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="209"/>
        <source>retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="109"/>
        <source>Advanced</source>
        <translation>ᠥᠨᠳᠥᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="114"/>
        <source>Type</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="132"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>ᠲᠠᠨ ᠳ᠋ᠤ᠌ ᠳᠤᠯᠠᠭᠠᠷᠠᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠯᠢᠭ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠭᠠᠷᠠᠢ᠂ ᠲᠠ ᠪᠠᠰᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠵᠤ᠂ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠦᠵᠡᠬᠦᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="134"/>
        <source>Remaining</source>
        <translation>ᠦᠯᠡᠳᠡᠵᠤ ᠪᠤᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="135"/>
        <source>character</source>
        <translation>ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="172"/>
        <source>Details</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="184"/>
        <source>ScreenShot</source>
        <translation>ᠵᠢᠷᠤᠭᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="200"/>
        <source>Add file</source>
        <translation>ᠹᠠᠢᠯ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="258"/>
        <source>The phone number cannot be empty</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠬᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="260"/>
        <source>The phone number format is incorrect</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="265"/>
        <source>Please enter your phone number</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="271"/>
        <source>appellation</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="279"/>
        <source>Contact</source>
        <translation>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="283"/>
        <source>Email required for anonymous feedback, not required for gitee feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="291"/>
        <source>The mailbox format is incorrect</source>
        <translation>ᠢᠮᠸᠯ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="297"/>
        <location filename="../UI/uiproblemfeedback.cpp" line="792"/>
        <source>Log in to gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="310"/>
        <source>Mailbox</source>
        <translation>ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="319"/>
        <source>Agree to take mine </source>
        <translation>ᠮᠢᠨᠦᠬᠢ ᠵᠢ ᠣᠯᠬᠤ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="329"/>
        <source>System information</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="355"/>
        <source>Submit</source>
        <translation>ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="372"/>
        <source>Details type</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="388"/>
        <source>Time period</source>
        <translation>ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ ᠤ᠋ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="423"/>
        <source>Information</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="426"/>
        <source>lately</source>
        <translation>ᠰᠠᠶᠢᠬᠠᠨ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="427"/>
        <source>days</source>
        <translation>ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="439"/>
        <source>YES</source>
        <translation>ᠳᠡᠢᠮᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="449"/>
        <source>NO</source>
        <translation>ᠪᠢᠰᠢ / ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="471"/>
        <source>Upload log</source>
        <translation>ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="477"/>
        <source>Path</source>
        <translation>ᠵᠢᠮ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="492"/>
        <source>Export to</source>
        <translation>ᠭᠠᠷᠭᠠᠭᠠᠳ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="605"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠢ᠋ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠣᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ 5 ᠹᠠᠢᠯ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠨᠡᠢᠳᠡ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ 10MB ᠡᠴᠡ ᠳᠠᠪᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="606"/>
        <source>Supported formats: </source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠵᠠᠭᠪᠤᠷ: </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="133"/>
        <source>Up to 500 characters</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠣᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ 500 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="237"/>
        <source>Files</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠤᠭᠤᠯᠤᠯ ᠢ ᠳᠡᠭᠡᠭ᠍ᠰᠢ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="683"/>
        <source>Repeat addition</source>
        <translation>ᠳᠠᠪᠬᠤᠴᠠᠵᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="699"/>
        <source>Attachment size out of limit</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯᠳᠠ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="809"/>
        <source>Log out of gitee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="810"/>
        <source>gitee has been associated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="914"/>
        <source>Add attachment</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="36"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="39"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="44"/>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="92"/>
        <source>Submitted successfully</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠢ᠋ ᠳᠤᠰᠢᠶᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="96"/>
        <source>Cancel successfully</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="100"/>
        <source>System is abnormal, contact technical support</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠠᠢᠬᠤ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠮᠡᠳᠡᠪᠡ᠂ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡᠨ ᠤ᠋ ᠬᠥᠮᠦᠰ ᠲᠠᠢ ᠰᠢᠭ᠋ᠤᠳ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="108"/>
        <source>Log and submission is packed, please go</source>
        <translation>ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠪᠤᠯᠤᠨ ᠳᠤᠰᠢᠶᠠᠭᠰᠠᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢ ᠪᠣᠭᠣᠳᠠᠯᠯᠠᠭᠰᠠᠨ᠂ ᠣᠴᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="108"/>
        <source>acquire</source>
        <translation>ᠣᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="106"/>
        <source>Submission failed</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠢ᠋ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="107"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>&apos; ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ&apos; ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠳᠠᠬᠢᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ᠂ ᠡᠰᠡᠪᠡᠯ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡᠨ ᠤ᠋ ᠬᠥᠮᠦᠰ ᠲᠠᠢ ᠰᠢᠭ᠋ᠤᠳ ᠬᠠᠷᠢᠯᠴᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="129"/>
        <source>Under submission...</source>
        <translation>ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="18"/>
        <source>Contact us</source>
        <translation>ᠪᠢᠳᠡᠨ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="39"/>
        <source>Mail</source>
        <translation>ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="52"/>
        <source>Community</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="62"/>
        <source>website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="69"/>
        <source> to get more services</source>
        <translation> ᠨᠡᠩ ᠣᠯᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠤ᠋ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠤᠢᠯᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="86"/>
        <source>Kylin technical services</source>
        <translation>ᠣᠯᠠᠨ ᠨᠡᠶᠢᠲᠡ ᠶᠢᠨ ᠨᠣᠮᠧᠷ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="61"/>
        <source>Jump to</source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠦᠰᠦᠷᠦᠬᠡᠳ</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="24"/>
        <source>Feedback</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤ ᠲᠤᠰᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="25"/>
        <source>Self service</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="27"/>
        <source>History</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠤ᠋ᠨ ᠳᠡᠤᠬᠡ</translation>
    </message>
</context>
</TS>
