<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>ConfigWin</name>
    <message>
        <location filename="../appUI/src/config_win.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="264"/>
        <source>SetInner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="429"/>
        <location filename="../appUI/src/config_win.cpp" line="61"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="464"/>
        <location filename="../appUI/src/config_win.cpp" line="62"/>
        <source>Save</source>
        <translation>Сақтау</translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.cpp" line="35"/>
        <location filename="../appUI/src/config_win.cpp" line="37"/>
        <source>IntraNetSet</source>
        <translation type="unfinished">IntraNetSet</translation>
    </message>
</context>
<context>
    <name>DHCPCheck</name>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="31"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="33"/>
        <source>DHCP Config</source>
        <translation>DHCP баптаулары</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="34"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="91"/>
        <source>Are DHCP config right?</source>
        <translation>DHCP конфигурациясы дұрыс па?</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="62"/>
        <source>Checking DHCP config</source>
        <translation>DHCP конфигурациясын тексеру</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="62"/>
        <source>Checking</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="69"/>
        <source>DHCP RUNNING RIGHT</source>
        <translation>DHCP RIGHT ІСКЕ ҚОСУ</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="69"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="78"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="73"/>
        <source>DHCP DISTRIBUTED WRONG IP</source>
        <translation>DHCP ТАРАТЫЛҒАН ҚАТЕ IP</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="73"/>
        <source>ERR</source>
        <translation>ЭРР</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="78"/>
        <source>DHCP IS OFF, NO CHECK</source>
        <translation>ЖҚЗ ҒЖ ӨШІРІЛГЕН, ТЕКСЕРУ ЖОҚ</translation>
    </message>
</context>
<context>
    <name>DNSCheck</name>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="29"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="32"/>
        <source>DNS Config</source>
        <translation>DNS баптаулары</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="33"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="44"/>
        <source>Are DNS config right?</source>
        <translation>DNS баптаулары дұрыс па?</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="56"/>
        <source>Checking DNS config</source>
        <translation>DNS конфигурациясын тексеру</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="56"/>
        <source>Checking</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <source>NO DNS</source>
        <translation type="unfinished">NO DNS</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="119"/>
        <source>ERR</source>
        <translation type="unfinished">ЭРР</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="119"/>
        <source>DNS service is working abnormally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="122"/>
        <source>DNS service is working properly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="122"/>
        <source>OK</source>
        <translation type="unfinished">ЖАҚСЫ</translation>
    </message>
</context>
<context>
    <name>DetailButton</name>
    <message>
        <location filename="../customWidget/detailbutton.cpp" line="31"/>
        <source>detail</source>
        <translation>егжей-тегжей</translation>
    </message>
</context>
<context>
    <name>HWCheck</name>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="31"/>
        <location filename="../HWCheck/hwcheck.cpp" line="33"/>
        <source>HardWare</source>
        <translation>HardWare</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="32"/>
        <location filename="../HWCheck/hwcheck.cpp" line="94"/>
        <source>Are network card OK and cable connected?</source>
        <translation>Желілік карта мен кабель қосылған ба?</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="64"/>
        <source>Checking NetWork HardWares</source>
        <translation>NetWork HardWares тексеру</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="64"/>
        <source>Checking</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="70"/>
        <source>NetWork HardWares are OK,Primary Wired.</source>
        <translation>NetWork HardWares — OK, Primary Wired.</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="70"/>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <source>NetWork HardWares are OK,Primary Wireless.</source>
        <translation>NetWork HardWares — OK, Primary Wireless.</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="78"/>
        <source>NetWork HardWares are OK, but no connection</source>
        <translation>NetWork HardWares жарайды, бірақ қосылымы жоқ</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="78"/>
        <location filename="../HWCheck/hwcheck.cpp" line="81"/>
        <source>ERR</source>
        <translation>ЭРР</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="81"/>
        <source>No valid net card</source>
        <translation>Жарамды нетто-карта жоқ</translation>
    </message>
</context>
<context>
    <name>HostCheck</name>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="35"/>
        <location filename="../HostCheck/hostcheck.cpp" line="37"/>
        <source>Host File</source>
        <translation>Хост файлы</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="38"/>
        <location filename="../HostCheck/hostcheck.cpp" line="302"/>
        <source>Are Host File config right?</source>
        <translation>Хост файлы дұрыс па?</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="57"/>
        <source>No host file!</source>
        <translation>Хост файлы жоқ!</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="73"/>
        <location filename="../HostCheck/hostcheck.cpp" line="74"/>
        <source>Has no sperated line.</source>
        <translation>Сперацияланған сызығы жоқ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="82"/>
        <location filename="../HostCheck/hostcheck.cpp" line="83"/>
        <location filename="../HostCheck/hostcheck.cpp" line="192"/>
        <location filename="../HostCheck/hostcheck.cpp" line="193"/>
        <source>Ipv4 localhost error.</source>
        <translation>Ipv4 localhost қатесі.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="99"/>
        <location filename="../HostCheck/hostcheck.cpp" line="100"/>
        <location filename="../HostCheck/hostcheck.cpp" line="196"/>
        <location filename="../HostCheck/hostcheck.cpp" line="197"/>
        <source>Ipv4 localPChost error.</source>
        <translation>Ipv4 localPChost қатесі.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="107"/>
        <location filename="../HostCheck/hostcheck.cpp" line="108"/>
        <source>Ipv6 localhost error.</source>
        <translation>Ipv6 localhost қатесі.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="115"/>
        <location filename="../HostCheck/hostcheck.cpp" line="116"/>
        <source>Ipv6 localnet error.</source>
        <translation>Ipv6 localnet қатесі.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="123"/>
        <location filename="../HostCheck/hostcheck.cpp" line="124"/>
        <source>Ipv6 mcastsprefix error.</source>
        <translation>Ipv6 mcastsprefix қатесі.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="131"/>
        <location filename="../HostCheck/hostcheck.cpp" line="132"/>
        <source>Ipv6 nodes error.</source>
        <translation>Ipv6 түйіндері қатесі.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="139"/>
        <location filename="../HostCheck/hostcheck.cpp" line="140"/>
        <source>Ipv6 routers error.</source>
        <translation>Ipv6 жол жоспарлағыш қатесі.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="157"/>
        <location filename="../HostCheck/hostcheck.cpp" line="158"/>
        <source>User add illegal hosts.</source>
        <translation>Пайдаланушы заңсыз хосттарды қосады.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="277"/>
        <source>Checking Host Files</source>
        <translation>Хост файлдарын тексеру</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="277"/>
        <source>Checking</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="282"/>
        <source>Hosts Files are OK</source>
        <translation>Хосттар файлдары жарайды</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="282"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="285"/>
        <location filename="../HostCheck/hostcheck.cpp" line="288"/>
        <source>ERR</source>
        <translation>ЭРР</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="288"/>
        <source>The local hosts file is abnormal, please repait it</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IPCheck</name>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="34"/>
        <location filename="../IPCheck/ipcheck.cpp" line="36"/>
        <source>IP Config</source>
        <translation>IP баптаулары</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="35"/>
        <location filename="../IPCheck/ipcheck.cpp" line="88"/>
        <source>Are IP config right?</source>
        <translation>IP баптаулары дұрыс па?</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="62"/>
        <source>Checking IP config</source>
        <translation>IP баптауларын тексеру</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="62"/>
        <source>Checking</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="67"/>
        <source>DHCP ON</source>
        <translation>ЖҚЗ ҒЖ</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="67"/>
        <location filename="../IPCheck/ipcheck.cpp" line="73"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="70"/>
        <source>IP CONFIG FALSE</source>
        <translation>IP КОНФИГУРАЦИЯСЫ FALSE</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="70"/>
        <source>ERR</source>
        <translation>ЭРР</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="73"/>
        <source>IP CONFIG RIGHT</source>
        <translation>IP БАПТАУЛАРЫ</translation>
    </message>
</context>
<context>
    <name>IPWebWidget</name>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="55"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="57"/>
        <source>Website</source>
        <translation>Веб-сайт</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="101"/>
        <source>Format error,IP is invalid</source>
        <translation>Пішім қатесі, IP жарамсыз</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="114"/>
        <source>Format error,web is invalid</source>
        <translation>Пішім қатесі, веб жарамсыз</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.ui" line="93"/>
        <source>Addr</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IncreaseWidget</name>
    <message>
        <location filename="../appUI/src/increase_widget.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemWidget</name>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="37"/>
        <source>Detail</source>
        <translation>егжей-тегжей</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="95"/>
        <source>Checking</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="99"/>
        <location filename="../customWidget/item_widget.cpp" line="111"/>
        <location filename="../customWidget/item_widget.cpp" line="119"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="103"/>
        <location filename="../customWidget/item_widget.cpp" line="115"/>
        <location filename="../customWidget/item_widget.cpp" line="123"/>
        <source>ERR</source>
        <translation>ЭРР</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="107"/>
        <location filename="../customWidget/item_widget.cpp" line="127"/>
        <source>WARNING</source>
        <translation>ЕСКЕРТУ</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <location filename="../libNWDBus/src/kylin-dbus-interface.cpp" line="927"/>
        <source>Wired connection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="32"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="358"/>
        <source>PushButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="426"/>
        <location filename="../appUI/src/mainwindow.cpp" line="60"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="451"/>
        <location filename="../appUI/src/mainwindow.cpp" line="64"/>
        <source>Restart</source>
        <translation>Қайта іске қосу</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="453"/>
        <source>Checking...</source>
        <translation>Тексеру...</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="670"/>
        <source>Intranet IP</source>
        <translation>Интранет IP</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="693"/>
        <source>Intranet Web</source>
        <translation>Intranet Web</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="401"/>
        <location filename="../appUI/src/mainwindow.cpp" line="454"/>
        <source>Start</source>
        <translation>Бастау</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="77"/>
        <source>Return</source>
        <translation>Қайтарым</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="128"/>
        <source>NetCheck</source>
        <translation>NetCheck</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="134"/>
        <source>total 6 items</source>
        <translation>барлығы 6 атау</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="488"/>
        <source>Canceling...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="727"/>
        <source>InnerNet Check</source>
        <translation>InnerNet тексеруі</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="728"/>
        <source>Check whether the intranet is smooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="731"/>
        <source>Internet access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="732"/>
        <source>Can user browse out net?</source>
        <translation>Пайдаланушы торды шолып шыға ала ма?</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="761"/>
        <source>checked %1 items, no issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="763"/>
        <location filename="../appUI/src/mainwindow.cpp" line="776"/>
        <source>checked %1 items, find %2 errs, %3 issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="768"/>
        <source>checked %1 items, find %2 errs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="772"/>
        <source>checked %1 items, find %2 issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="903"/>
        <source>Check interrupted, no issues found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="904"/>
        <source>We suggest that you conduct a complete inspection again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="908"/>
        <source>Check interruption and found %1 issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="909"/>
        <source>Please repair and retest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="917"/>
        <source>No problems found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="918"/>
        <source>Please continue to maintain and regularly check up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="923"/>
        <location filename="../appUI/src/mainwindow.cpp" line="939"/>
        <source>Found %1 problem and %2 prompt problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="924"/>
        <location filename="../appUI/src/mainwindow.cpp" line="929"/>
        <location filename="../appUI/src/mainwindow.cpp" line="934"/>
        <location filename="../appUI/src/mainwindow.cpp" line="940"/>
        <source>Please re-detect after repair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="928"/>
        <source>Found %1 problem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="933"/>
        <source>Found %1 prompt problem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="290"/>
        <location filename="../appUI/src/mainwindow.cpp" line="47"/>
        <location filename="../appUI/src/mainwindow.cpp" line="829"/>
        <source>Detect and resolve Network Faults</source>
        <translation>Желі ақауларын анықтау және жою</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="249"/>
        <location filename="../appUI/src/mainwindow.cpp" line="44"/>
        <location filename="../appUI/src/mainwindow.cpp" line="830"/>
        <source>Detect Network Faults</source>
        <translation>Желінің ақаулықтарын анықтау</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="66"/>
        <location filename="../appUI/src/menumodule.cpp" line="102"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="68"/>
        <location filename="../appUI/src/menumodule.cpp" line="100"/>
        <source>About</source>
        <translation>Шамамен</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="70"/>
        <location filename="../appUI/src/menumodule.cpp" line="105"/>
        <source>Configure</source>
        <translation>Баптау</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="72"/>
        <location filename="../appUI/src/menumodule.cpp" line="98"/>
        <source>Quit</source>
        <translation>Шығу</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="135"/>
        <source>Network-check-tool is a software that can quickly detect,diagnose,and optimize networks.</source>
        <translation>Желілік-бақылау-құрал - желілерді тез анықтауға, диагностикалауға және оңтайландыруға қабілетті бағдарламалық жасақтама.</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="134"/>
        <location filename="../appUI/src/menumodule.cpp" line="259"/>
        <source>Version: </source>
        <translation>Нұсқасы: </translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="263"/>
        <source>Network-check-tool is a software that can quickly detect, diagnose, and optimize networks. </source>
        <translation>Желілік бақылау-құрал - желілерді тез анықтауға, диагностикалауға және оңтайландыруға қабілетті бағдарламалық қамтамасыз ету. </translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="275"/>
        <location filename="../appUI/src/menumodule.cpp" line="358"/>
        <location filename="../appUI/src/menumodule.cpp" line="366"/>
        <source>Service &amp; Support: </source>
        <translation>Қызмет және қолдау: </translation>
    </message>
</context>
<context>
    <name>NetCheck</name>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="54"/>
        <location filename="../NetCheck/netcheck.cpp" line="55"/>
        <location filename="../NetCheck/netcheck.cpp" line="77"/>
        <location filename="../NetCheck/netcheck.cpp" line="78"/>
        <source>InnerNet Check</source>
        <translation>InnerNet тексеруі</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="56"/>
        <location filename="../NetCheck/netcheck.cpp" line="76"/>
        <source>Can user browse inner net?</source>
        <translation>Пайдаланушы ішкі торды шола ала ма?</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="58"/>
        <location filename="../NetCheck/netcheck.cpp" line="59"/>
        <location filename="../NetCheck/netcheck.cpp" line="81"/>
        <location filename="../NetCheck/netcheck.cpp" line="82"/>
        <source>AccessNet Check</source>
        <translation>AccessNetтексеруі</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="60"/>
        <location filename="../NetCheck/netcheck.cpp" line="80"/>
        <source>Can user browse out net?</source>
        <translation>Пайдаланушы торды шолып шыға ала ма?</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="76"/>
        <location filename="../NetCheck/netcheck.cpp" line="80"/>
        <source>Checking</source>
        <translation>Тексеру</translation>
    </message>
</context>
<context>
    <name>NetCheckHomePage</name>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="29"/>
        <source>Check and Repair</source>
        <translation>Тексеру және жөндеу</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="36"/>
        <source>Detection and repair of computer problems</source>
        <translation>Компьютердің ақаулықтарын анықтау және жөндеу</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="49"/>
        <source>NetCheck</source>
        <translation>NetCheck</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="66"/>
        <source>Start</source>
        <translation>Бастау</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="97"/>
        <source>IntraNetSet</source>
        <translation>IntraNetSet</translation>
    </message>
</context>
<context>
    <name>NetCheckThread</name>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="311"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="314"/>
        <source>Extranet normal</source>
        <translation type="unfinished">Экстранет қалыпты</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="311"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="314"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="324"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="329"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="345"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <source>OK</source>
        <translation type="unfinished">ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="317"/>
        <source>Extranet abnormal</source>
        <translation type="unfinished">Экстранет аномальды</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="317"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="342"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="360"/>
        <source>ERR</source>
        <translation type="unfinished">ЭРР</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="324"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="329"/>
        <source>Intranet normal</source>
        <translation type="unfinished">Интранет қалыпты</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <source>Url can be accessed</source>
        <translation type="unfinished">url мекенжайына кіруге болады</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="342"/>
        <source>Url cannot be accessed</source>
        <translation type="unfinished">Url мекенжайына кіру мүмкін емес</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <source>IP is reachable</source>
        <translation type="unfinished">IP қол жетімді</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="345"/>
        <source>IP is reachable，url cannot be accessed</source>
        <translation type="unfinished">IP қол жетімді, url- ге кіру мүмкін емес</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <source>IP is unreachable</source>
        <translation type="unfinished">IP қол жетімді емес</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <source>IP is unreachable，url can be accessed</source>
        <translation type="unfinished">IP қол жеткізілмейді, url мекенжайына кіруге болады</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="360"/>
        <source>IP is unreachable，url cannot be accessed</source>
        <translation type="unfinished">IP қол жеткізе алмайды, url мекенжайына кіру мүмкін емес</translation>
    </message>
</context>
<context>
    <name>ProxyCheck</name>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="38"/>
        <source>NetWork Proxy</source>
        <translation>NetWork проксиі</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Checking</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="40"/>
        <source>Proxy</source>
        <translation>Прокси</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="41"/>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Check whether the proxy is working?</source>
        <translation>Проксидің жұмыс істеп тұрғанын тексеріңіз бе?</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="71"/>
        <source>proxy disable</source>
        <translation>прокси ажыратылсын</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="74"/>
        <source>auto proxy normal</source>
        <translation>авто прокси қалыпты</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="77"/>
        <source>auto proxy abnormal</source>
        <translation>автокси аномальды</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="80"/>
        <source>manual proxy normal</source>
        <translation>қол прокси қалыпты</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appUI/src/netcheckentr.cpp" line="34"/>
        <source>NetworkCheck</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuadBtnsTitleBar</name>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="30"/>
        <source>menu</source>
        <translation>мәзірі</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="38"/>
        <source>minimize</source>
        <translation>кішірейту</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="51"/>
        <source>full screen</source>
        <translation>толық экран</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="64"/>
        <source>close</source>
        <translation>жабу</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="83"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="85"/>
        <source>kylin network applet desktop message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="38"/>
        <source>WLAN Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="49"/>
        <source>Input WLAN Information Please</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="50"/>
        <source>WLAN ID：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="51"/>
        <source>WLAN Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="52"/>
        <source>Password：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="53"/>
        <source>Cancl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="54"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
