<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AppItem</name>
    <message>
        <location filename="../appitem.cpp" line="114"/>
        <source>coming soon</source>
        <translation>ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../appitem.cpp" line="115"/>
        <source>More tools are coming soon</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠵᠢᠵᠢᠭ ᠪᠠᠭᠠᠵᠢ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugininterface.cpp" line="35"/>
        <source>ToolBox</source>
        <translation>ᠪᠠᠭᠠᠵᠢ ᠵᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <location filename="../toolboxwidget.cpp" line="64"/>
        <source>My Tool</source>
        <translation>ᠮᠢᠨᠤ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../toolboxwidget.cpp" line="68"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>ᠡᠯ᠎ᠡ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠵᠢᠵᠢᠭ ᠪᠠᠭᠠᠵᠢ ᠲᠠᠨ ᠳ᠋ᠤ᠌ ᠬᠠᠪᠰᠤᠷᠴᠤ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠢ᠋ ᠨᠡᠩ ᠰᠠᠢᠨ ᠠᠰᠢᠭ᠋ᠯᠠᠭᠤᠯᠬᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
</context>
</TS>
