#ifndef KYLIN_OS_MANAGER_KOM_UTILS_H
#define KYLIN_OS_MANAGER_KOM_UTILS_H

namespace kom {

class KomUtils {
public:
    static double adaptFontSize(double pixelSize = 14.0);
};

}

#endif
