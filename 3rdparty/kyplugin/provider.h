/*
 * Pluma - Plug-in Management Framework
 * Copyright (C) 2010-2012 Gil Costa (gsaurus@gmail.com)
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 *    you must not claim that you wrote the original software.
 *    If you use this software in a product, an acknowledgment
 *    in the product documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 *    and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef KYPLUGIN_PROVIDER_H_
#define KYPLUGIN_PROVIDER_H_

#include <string>

namespace kyplugin
{

class Host;

class Provider
{
    friend class Host;

public:
    virtual ~Provider();
    virtual unsigned int getVersion() const = 0;
    bool isCompatible(const Host &host) const;

private:
    virtual std::string kypluginGetType() const = 0;
};

} // namespace kyplugin

#endif