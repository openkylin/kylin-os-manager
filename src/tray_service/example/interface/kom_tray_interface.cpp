#include "kom_tray_interface.h"

const unsigned int KomTrayProvider::providerVersion = 1;
const unsigned int KomTrayProvider::providerLowestVersion = 1;
const std::string KomTrayProvider::providerType = "KomTrayProvider";
