<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="47"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠨᠢᠭᠡᠨ ᠭᠠᠷᠴᠠᠭ ᠪᠤᠶᠤ ᠡᠷᠬᠡ ᠦᠬᠡᠢ᠂ ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="50"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="45"/>
        <source>Are you sure to start crushing?</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="51"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="58"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="66"/>
        <source>cancle</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>kylin file crush</source>
        <translation>ᠹᠠᠢᠯ ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠤᠷ</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <source>FileShredder</source>
        <translation type="vanished">ᠹᠠᠢᠯ ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="35"/>
        <location filename="../shreddialog.cpp" line="38"/>
        <source>File Shredder</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠤᠨᠲᠠᠭ ᠤᠨ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="60"/>
        <source>No file selected to be shredded</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="159"/>
        <location filename="../shreddialog.cpp" line="204"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>ᠹᠠᠢᠯ ᠪᠦᠷᠢᠮᠦᠰᠦᠨ ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠳᠠᠵᠤ᠂ ᠳᠠᠬᠢᠨ ᠰᠡᠷᠬᠦᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="160"/>
        <source>Shred File</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="161"/>
        <source>Deselect</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠵᠤ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="162"/>
        <source>Add File</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="163"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation>ᠠᠩᠬᠠᠷᠤᠭᠠᠷᠠᠢ: ᠹᠠᠢᠯ ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠪᠤᠯᠭᠤᠮᠵᠢᠳᠠᠢ ᠠᠵᠢᠯᠯᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="195"/>
        <source>Select file</source>
        <translation>ᠹᠠᠢᠯ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="195"/>
        <source>All Files(*)</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ (*)</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="236"/>
        <source>File Shredding ...</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠤᠨᠳᠠᠭᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="259"/>
        <source>Shred successfully!</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠤᠨᠳᠠᠭᠯᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="273"/>
        <source>Shred failed!</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠤᠨᠳᠠᠭᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="295"/>
        <source>Please select the file to shred!</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="300"/>
        <source>You did not select a file with permissions!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠨᠢ ᠨᠢᠭᠡᠨ ᠡᠷᠬᠡ ᠲᠠᠢ ᠹᠠᠢᠯ ᠪᠢᠰᠢ!</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="49"/>
        <source>Shred Manager</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="54"/>
        <source>Delete files makes it unable to recover</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
</context>
</TS>
