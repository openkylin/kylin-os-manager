<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug_CN">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="47"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>سىز تاللىغان ھۆججەت مۇندەرىجە ياكى ئىجازەتنامىسى يوق، پارچىلاشقا بولمايدۇ!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="50"/>
        <source>sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="45"/>
        <source>Are you sure to start crushing?</source>
        <translation>سىز چوقۇم چېقىشنى باشلامسىز؟</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="51"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>پارچىلانغان ھۆججەتلەرنى ئەسلىگە كەلتۈرگىلى بولمايدۇ!</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="58"/>
        <source>sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="66"/>
        <source>cancle</source>
        <translation>cancle</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>kylin file crush</source>
        <translation>Kylin Shred باشقۇرغۇ</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <source>FileShredder</source>
        <translation type="vanished">Kylin Shred باشقۇرغۇ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="35"/>
        <location filename="../shreddialog.cpp" line="38"/>
        <source>File Shredder</source>
        <translation>Kylin Shred باشقۇرغۇ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="60"/>
        <source>No file selected to be shredded</source>
        <translation>پارچىلاشقا تاللانغان ھۆججەت يوق</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="159"/>
        <location filename="../shreddialog.cpp" line="204"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>ھۆججەت پۈتۈنلەي پارچىلىنىپ، ئەسلىگە كەلتۈرگىلى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="160"/>
        <source>Shred File</source>
        <translation>Shred ھۆججەت</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="161"/>
        <source>Deselect</source>
        <translation>دېسكىغا ئېلىش</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="162"/>
        <source>Add File</source>
        <translation>ھۆججەت قوشۇش</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="163"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation>ئەسكەرتىش: ھۆججەت پارچىلاش جەريانىنى بىكار قىلىشقا بولمايدۇ، ئېھتىيات بىلەن مەشغۇلات قىلىڭ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="195"/>
        <source>Select file</source>
        <translation>ھۆججەت تاللاش</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="195"/>
        <source>All Files(*)</source>
        <translation>بارلىق ھۆججەتلەر(*)</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="236"/>
        <source>File Shredding ...</source>
        <translation>ھۆججەت Shredding ...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="259"/>
        <source>Shred successfully!</source>
        <translation>مۇۋەپپىقىيەتلىك قىرغىن قىل!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="273"/>
        <source>Shred failed!</source>
        <translation>Shred مەغلۇپ بولدى!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="295"/>
        <source>Please select the file to shred!</source>
        <translation>پارچىلاش ئۈچۈن ھۆججەتنى تاللاپ كۆرۈڭ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="300"/>
        <source>You did not select a file with permissions!</source>
        <translation>سىز ئىجازەتنامىسى بار ھۆججەتنى تاللىمىدىڭىز!</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="49"/>
        <source>Shred Manager</source>
        <translation>Shred باشقۇرغۇ</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="54"/>
        <source>Delete files makes it unable to recover</source>
        <translation>ھۆججەتلەرنى ئۆچۈرۈش ئارقىلىق ئەسلىگە كەلتۈرگىلى بولمايدۇ</translation>
    </message>
</context>
</TS>
