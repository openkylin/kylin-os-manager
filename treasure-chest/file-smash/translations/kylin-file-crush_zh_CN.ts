<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>格式</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="47"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>您选择的文件是一个目录或者无权限，无法粉碎！</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="50"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="45"/>
        <source>Are you sure to start crushing?</source>
        <translation>是否确定开始粉碎？</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="51"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>粉碎的文件将不可恢复！</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="58"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="66"/>
        <source>cancle</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>kylin file crush</source>
        <translation>文件粉碎机</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <source>FileShredder</source>
        <translation type="vanished">文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="35"/>
        <location filename="../shreddialog.cpp" line="38"/>
        <source>File Shredder</source>
        <translation>文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="60"/>
        <source>No file selected to be shredded</source>
        <translation>未选择要粉碎的文件</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="159"/>
        <location filename="../shreddialog.cpp" line="204"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>文件将被完全粉碎且不能恢复</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="160"/>
        <source>Shred File</source>
        <translation>粉碎</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="161"/>
        <source>Deselect</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="162"/>
        <source>Add File</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="163"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation>注意：文件粉碎操作不可取消，请谨慎操作！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="195"/>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="195"/>
        <source>All Files(*)</source>
        <translation>所有文件(*)</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="236"/>
        <source>File Shredding ...</source>
        <translation>文件粉碎中...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="259"/>
        <source>Shred successfully!</source>
        <translation>文件粉碎成功！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="273"/>
        <source>Shred failed!</source>
        <translation>文件粉碎失败！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="295"/>
        <source>Please select the file to shred!</source>
        <translation>请选择要粉碎的文件！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="300"/>
        <source>You did not select a file with permissions!</source>
        <translation>您选择的不是一个有权限的文件！</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="49"/>
        <source>Shred Manager</source>
        <translation>文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="54"/>
        <source>Delete files makes it unable to recover</source>
        <translation>粉碎文件将无法恢复！</translation>
    </message>
</context>
</TS>
