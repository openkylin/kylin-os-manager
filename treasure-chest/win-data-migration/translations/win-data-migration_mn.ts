<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QApplication</name>
    <message>
        <source>Win Data Migration</source>
        <translation>win ᠨᠡᠭᠦᠯᠭᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>ok</source>
        <translation>ok</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <source>failed to migrate files</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <source>view</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>failed to read the migration log:</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠭᠰᠡᠨ ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠤᠩᠰᠢᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ:</translation>
    </message>
    <message>
        <source>migration log</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠭᠰᠡᠨ ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <source>how to set the win</source>
        <translation>win ᠦᠵᠦᠬᠦᠷ ᠢ᠋ ᠬᠡᠷᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>how to set the win (required reading)</source>
        <translation>win ᠦᠵᠦᠬᠦᠷ ᠢ᠋ ᠬᠡᠷᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ( ᠡᠷᠬᠡᠪᠰᠢ ᠤᠩᠰᠢᠬᠤ)</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation>IP ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <source>win work group</source>
        <translation>win ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠪᠦᠯᠬᠦᠮ</translation>
    </message>
    <message>
        <source>win user name</source>
        <translation>win ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>win password</source>
        <translation>win ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <source>establish connection</source>
        <translation>ᠴᠥᠷᠬᠡᠯᠡᠭᠡ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>successful connection</source>
        <translation>ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>connecting...</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>connection failed, please try again</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>select the files you want to migrate</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠬᠦ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>file migration to</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠦᠯᠬᠡᠬᠡᠳ</translation>
    </message>
    <message>
        <source>start migration</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠨᠡᠬᠦᠯᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>select migration directory</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <source>migration success</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <source>failed to create migration logs</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠬᠦ ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>files failed to be migrated</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠳ ᠢ᠋ ᠨᠡᠬᠦᠯᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>no migration items are selected</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠬᠦ ᠶᠠᠮᠠᠷᠪᠠ ᠳᠦᠷᠦᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>save path is invalid or unauthorized</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢᠮ ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠪᠤᠶᠤ ᠡᠷᠬᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>file creation failure</source>
        <translation>ᠹᠠᠢᠯ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>file integrity check fails</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠪᠦᠷᠢᠨ ᠪᠦᠳᠦᠨ ᠴᠢᠨᠠᠷ ᠢ᠋ ᠬᠠᠷᠭᠤᠭᠤᠯᠤᠨ ᠬᠢᠨᠠᠭᠠᠳ ᠬᠢᠷᠢ ᠳᠡᠩᠴᠡᠭᠰᠡᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>[loading]</source>
        <translation>[ ᠠᠴᠢᠶᠠᠯᠠᠵᠤ ᠪᠤᠢ]</translation>
    </message>
    <message>
        <source>smb disconnected abnormally</source>
        <translation>smb ᠴᠥᠷᠬᠡᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠡᠷ ᠳᠠᠰᠤᠷᠪᠠ</translation>
    </message>
    <message>
        <source>loading migration file tree</source>
        <translation>ᠨᠡᠬᠦᠯᠬᠡᠬᠦ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠤᠩᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <source>&lt;h2&gt;win Settings &lt;/h2&gt;&lt;p&gt; Step 1: Both computers need to be in the same LAN network &lt;/p&gt;&lt;p&gt; Step 2: win computer needs to set the folder or file as a shared folder &lt;/p&gt;&lt;div&gt;1. Select the folder to share.&lt;/div&gt;&lt;div&gt;2. Right click on Properties and select the Sharing tab. Click on Advanced Sharing Settings, check Share this folder, and then click Apply.&lt;/div&gt;</source>
        <translation>&lt;h2&gt;win ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ &lt;/h2&gt;&lt;p&gt; ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠠᠯᠬᠤᠮ: ᠬᠣᠶᠠᠷ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠡᠰᠡᠭ ᠲᠡᠬᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠤᠳᠤᠷ᠎ᠠ &lt;/p&gt;&lt;p&gt; ᠬᠣᠶᠠᠳᠤᠭᠠᠷ ᠠᠯᠬᠤᠮ: win ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠨᠢ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ &lt;/div&gt;&lt;div&gt;1 ᠪᠣᠯᠭᠠᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ᠂ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠵᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ᠃&lt;/div&gt;&lt;div&gt;2᠂ ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ ᠤ᠋ᠨ ᠪᠠᠷᠠᠭᠤᠨ ᠳᠠᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠢ᠋ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ᠂ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠤ᠋ᠨ ᠱᠣᠰᠢᠭ᠋᠎ᠠ ᠵᠢ ᠰᠣᠩᠭᠣᠵᠤ᠂ ᠥᠨᠳᠥᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠨ᠂ ᠲᠤᠰ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠵᠢ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠪᠡᠷ ᠭᠣᠬᠠᠳᠠᠵᠤ᠂ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠨ᠎ᠠ.&lt;/div&gt;</translation>
    </message>
    <message>
        <source>Canceling file migration, please wait ……</source>
        <translation>ᠹᠠᠢᠯ ᠨᠡᠬᠦᠯᠬᠡᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ……</translation>
    </message>
    <message>
        <source>Win Data Migration</source>
        <translation>win ᠨᠡᠭᠦᠯᠭᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
</context>
</TS>
