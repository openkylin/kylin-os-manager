<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>QApplication</name>
    <message>
        <source>Win Data Migration</source>
        <translation>ٴجۇرىس اۋىستىرۋ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Win Data Migration</source>
        <translation>ٴجۇرىس اۋىستىرۋ</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>تۇراقتاندىرۋ</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>كۇشىنەن قالدىرۋ</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>قايتا سىناۋ</translation>
    </message>
    <message>
        <source>failed to migrate files</source>
        <translation>ٴساتسىز كوشىرىلگەن قۇجات</translation>
    </message>
    <message>
        <source>view</source>
        <translation>قاراۋ</translation>
    </message>
    <message>
        <source>failed to read the migration log:</source>
        <translation>وقۋ ناتيجەسى :</translation>
    </message>
    <message>
        <source>migration log</source>
        <translation>كۇندەلىك تۇرمىس</translation>
    </message>
    <message>
        <source>how to set the win</source>
        <translation>ايىرىق شىعارۋ</translation>
    </message>
    <message>
        <source>how to set the win (required reading)</source>
        <translation>( قالاي ورنالاستىرىلماسىن؟</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation>توراپ ادرەستى</translation>
    </message>
    <message>
        <source>win work group</source>
        <translation>قىزمەت گرۋپپاسى</translation>
    </message>
    <message>
        <source>win user name</source>
        <translation>تۇتىنۋشى</translation>
    </message>
    <message>
        <source>win password</source>
        <translation>قۇپيا سيفىرلى</translation>
    </message>
    <message>
        <source>establish connection</source>
        <translation>جالعاۋ</translation>
    </message>
    <message>
        <source>successful connection</source>
        <translation>ۇلاسىمدى ەتىپ قوسۋ ارقىلى تابىسقا جەتۋ</translation>
    </message>
    <message>
        <source>connecting...</source>
        <translation>قازىر ۇلاسۋ ۇستىندە.</translation>
    </message>
    <message>
        <source>connection failed, please try again</source>
        <translation>جالعاسۋ</translation>
    </message>
    <message>
        <source>select the files you want to migrate</source>
        <translation>ماڭىزدى قۇجاتتار مەن</translation>
    </message>
    <message>
        <source>file migration to</source>
        <translation>كوشىپ كەلۋ</translation>
    </message>
    <message>
        <source>start migration</source>
        <translation>كوشۋ</translation>
    </message>
    <message>
        <source>select migration directory</source>
        <translation>سۇرىپتاۋ جانە كوشىرۋ بارىسىندا، تاڭدالانىو</translation>
    </message>
    <message>
        <source>migration success</source>
        <translation>كوشۋ</translation>
    </message>
    <message>
        <source>failed to create migration logs</source>
        <translation>قۇرۋ ارقىلى ساتسىزدىككە ۇشىراعاندا،</translation>
    </message>
    <message>
        <source>files failed to be migrated</source>
        <translation>جەكە دارا قۇجات بويىنشا بولعاندا،</translation>
    </message>
    <message>
        <source>no migration items are selected</source>
        <translation>ٴارقانداي ٴبىر</translation>
    </message>
    <message>
        <source>save path is invalid or unauthorized</source>
        <translation>ساقتاپ قالۋ جولى زاڭسىز نەمەسە ۇقىق شەگى جوق</translation>
    </message>
    <message>
        <source>file creation failure</source>
        <translation>قۇجات قۇرۋ بارىسىنداعى اعاتتىق</translation>
    </message>
    <message>
        <source>file integrity check fails</source>
        <translation>دەرەكجيىن اشىپ، تەكسەرۋ، سالىستىرۋ ارقىلى تەكسەرۋ</translation>
    </message>
    <message>
        <source>[loading]</source>
        <translation>{ورتاشا ەسەپپەن 2}</translation>
    </message>
    <message>
        <source>smb disconnected abnormally</source>
        <translation>sanalarandisali</translation>
    </message>
    <message>
        <source>loading migration file tree</source>
        <translation>وقۋشىنىڭ اۋىسۋى جونىندەگى حۇجات شىعارۋ</translation>
    </message>
    <message>
        <source>&lt;h2&gt;win Settings &lt;/h2&gt;&lt;p&gt; Step 1: Both computers need to be in the same LAN network &lt;/p&gt;&lt;p&gt; Step 2: win computer needs to set the folder or file as a shared folder &lt;/p&gt;&lt;div&gt;1. Select the folder to share.&lt;/div&gt;&lt;div&gt;2. Right click on Properties and select the Sharing tab. Click on Advanced Sharing Settings, check Share this folder, and then click Apply.&lt;/div&gt;</source>
        <translation>&lt;h2&gt;قولتاڭبا باسۋ &lt;/h2&gt;&lt;p&gt; ءبىرىنشى قادامدا كومپيۋتەر ەكىلىك ەسەپتەۋ ماشيناسىمەن ۇقساس وڭىرلىك توراپ بەكەتتەرى : &lt;/p&gt;&lt;p&gt; ەكىنشى باسقىشقا : ورتاق قولدانىلاتىن بوقشا، ورتاق پايدالانىلاتىن ءۇي شارۋاسى، ورتاق پايدالانىلاتىن ءۇي، ورتاق پايدالانىلاتىن ءۇي، ورتاق پايدالانىلاتىن ءۇي، ورتاق پايدالانىلاتىن ءۇي، ورتاق پايدالانىلاتىن ءۇي، &lt;/p&gt;&lt;div&gt;1. ورتاق پايدالانىلاتىن قۇجات&lt;/div&gt;&lt;div&gt;2تۇيمەلەپ ورتاق پايدالانىلاتىن ءۇي توبەسىندە، جوعارى دارەجەلى ورىن تاڭداپ الىپ،&lt;/div&gt;</translation>
    </message>
    <message>
        <source>Canceling file migration, please wait ……</source>
        <translation>قۇجاتتار مەن بىرگە كوشىرىلىپ،</translation>
    </message>
</context>
</TS>
