<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>QApplication</name>
    <message>
        <source>Win Data Migration</source>
        <translation>утуп миграция куралы</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Win Data Migration</source>
        <translation>утуп миграция куралы</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>Албетте</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>Кайталап көрүңүз</translation>
    </message>
    <message>
        <source>failed to migrate files</source>
        <translation>Ийгиликтүү көчүрүлгөн эмес файлдар</translation>
    </message>
    <message>
        <source>view</source>
        <translation>Текшерүү</translation>
    </message>
    <message>
        <source>failed to read the migration log:</source>
        <translation>Миграция журналы окулбай калды:</translation>
    </message>
    <message>
        <source>migration log</source>
        <translation>миграция журналы</translation>
    </message>
    <message>
        <source>how to set the win</source>
        <translation>Жеңиш жагын кантип орнотуу керек</translation>
    </message>
    <message>
        <source>how to set the win (required reading)</source>
        <translation>Жеңиш тарапта кантип орнотуу керек (талап кылынган окуу)</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation>IP дареги</translation>
    </message>
    <message>
        <source>win work group</source>
        <translation>колдонуучу тобун утуп алыңыз</translation>
    </message>
    <message>
        <source>win user name</source>
        <translation>колдонуучунун атын утуп алыңыз</translation>
    </message>
    <message>
        <source>win password</source>
        <translation>сырсөз утуп алуу</translation>
    </message>
    <message>
        <source>establish connection</source>
        <translation>байланыш түзүү</translation>
    </message>
    <message>
        <source>successful connection</source>
        <translation>байланыш ийгиликтүү болду</translation>
    </message>
    <message>
        <source>connecting...</source>
        <translation>Туташууда...</translation>
    </message>
    <message>
        <source>connection failed, please try again</source>
        <translation>Туташуу ишке ашкан жок, кайра аракет кылыңыз</translation>
    </message>
    <message>
        <source>select the files you want to migrate</source>
        <translation>Көчүрүү үчүн файлдарды тандаңыз</translation>
    </message>
    <message>
        <source>file migration to</source>
        <translation>файлдар көчүрүлгөн</translation>
    </message>
    <message>
        <source>start migration</source>
        <translation>миграцияны баштоо</translation>
    </message>
    <message>
        <source>select migration directory</source>
        <translation>Миграция каталогун тандаңыз</translation>
    </message>
    <message>
        <source>migration success</source>
        <translation>Миграция аяктады</translation>
    </message>
    <message>
        <source>failed to create migration logs</source>
        <translation>Миграция журналы түзүлбөй калды</translation>
    </message>
    <message>
        <source>files failed to be migrated</source>
        <translation>файлдар көчүрүлгөн жок</translation>
    </message>
    <message>
        <source>no migration items are selected</source>
        <translation>Көчүрүү тандалган жок</translation>
    </message>
    <message>
        <source>save path is invalid or unauthorized</source>
        <translation>Сактоо жолу жараксыз же уруксаты жок</translation>
    </message>
    <message>
        <source>file creation failure</source>
        <translation>Файл түзүлбөй калды</translation>
    </message>
    <message>
        <source>file integrity check fails</source>
        <translation>Файлдын бүтүндүгүн текшерүү ишке ашкан жок</translation>
    </message>
    <message>
        <source>[loading]</source>
        <translation>[Жүктөө]</translation>
    </message>
    <message>
        <source>smb disconnected abnormally</source>
        <translation>SMB туташуусу анормалдуу үзгүлтүк</translation>
    </message>
    <message>
        <source>loading migration file tree</source>
        <translation>Миграциялык файл дарагын окуп чыгыңыз</translation>
    </message>
    <message>
        <source>&lt;h2&gt;win Settings &lt;/h2&gt;&lt;p&gt; Step 1: Both computers need to be in the same LAN network &lt;/p&gt;&lt;p&gt; Step 2: win computer needs to set the folder or file as a shared folder &lt;/p&gt;&lt;div&gt;1. Select the folder to share.&lt;/div&gt;&lt;div&gt;2. Right click on Properties and select the Sharing tab. Click on Advanced Sharing Settings, check Share this folder, and then click Apply.&lt;/div&gt;</source>
        <translation>&lt;h2&gt;утуп каптал орнотуулар &lt;/h2&gt;&lt;p&gt;1-кадам: Эки компьютер бир LAN тармагында болушу керек  &lt;/p&gt;&lt;p&gt; 2-кадам: Жеңүүчү компьютер папканы же файлды жалпы папка катары коюшу керек  &lt;/p&gt;&lt;div&gt;1. Бөлүшө турган папканы тандаңыз&lt;/div&gt;&lt;div&gt;2. Dinani pa batani lamanja la mbewa, ndipo sankhani kusindikiza tsamba, dinani pazomwe mukugawaniza, dinani pa fayilo kuti mugawane foda, dinani pa ntchito.&lt;/div&gt;</translation>
    </message>
    <message>
        <source>Canceling file migration, please wait ……</source>
        <translation>Kusamutsidwa kwa zikalata kumaletsedwa. Chonde dikirani...</translation>
    </message>
</context>
</TS>
